function zsl() {
  # Zellij select layout
  zlayout=".config/zellij/layouts/"
  zellij --layout $(ls $zlayout | sd ".kdl" "" | fzf)
}

function zel() {
  # Zellij edit layout
  zlayout=".config/zellij/layouts/"
  nvim "$zlayout$(ls $zlayout | fzf)"
}
