function Plugin_config(name)
  return function()
    require(string.format("plugin.config.%s", name))
  end
end

local plugin = {
  'terrastruct/d2-vim',
  'NoahTheDuke/vim-just',
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',
  'tpope/vim-sleuth',
  'preservim/vim-pencil',
  'axelf4/vim-strip-trailing-whitespace',
  {
    {
        "kdheepak/lazygit.nvim",
        dependencies =  {
            "nvim-telescope/telescope.nvim",
            "nvim-lua/plenary.nvim"
        },
        config = function()
            require("telescope").load_extension("lazygit")
        end,
    },
  },
  { 'numToStr/Comment.nvim', opts = {} },
  { 'folke/which-key.nvim', opts = {} },
  -- lazy.nvim
  { "folke/neodev.nvim", opts = {} },
  { 'lewis6991/gitsigns.nvim', config = Plugin_config("gitsign") },
  {'nvim-lualine/lualine.nvim', config = Plugin_config("lualine")},
  { "folke/noice.nvim",
    event = "VeryLazy", opts = {},
  dependencies = {"MunifTanjim/nui.nvim", "rcarriga/nvim-notify"}
  },
  {
    'neovim/nvim-lspconfig',
    dependencies = {
      { 'williamboman/mason.nvim', config = true }, 'williamboman/mason-lspconfig.nvim',
      { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },
    },
    config = Plugin_config("lspconfig")
  },

  {
    'hrsh7th/nvim-cmp',
    dependencies = { 'L3MON4D3/LuaSnip' , 'saadparwaiz1/cmp_luasnip', 'hrsh7th/cmp-nvim-lsp' },
    config = Plugin_config("cmp")
  },

  {
    'navarasu/onedark.nvim',
    priority = 1000,
    config = function()
      vim.cmd.colorscheme 'onedark'
    end,
  },
  {
    'nvim-telescope/telescope.nvim',
    branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim' },
    config = Plugin_config("telescope")
  },
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    cond = function()
      return vim.fn.executable 'make' == 1
    end,
  },
  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    build = ':TSUpdate',
    config = Plugin_config("treesitter")
  },
  {
      "kylechui/nvim-surround",
      version = "*", -- Recommends main
      event = "VeryLazy",
      config = function()
          require("nvim-surround").setup({})
      end
  },
  {
  "epwalsh/obsidian.nvim",
    version = "*",
    lazy = true,
    ft = "markdown",
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
    config = Plugin_config("obsidian")
  },
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function() vim.fn["mkdp#util#install"]() end,
  },
}

require("lazy").setup(plugin)
