vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })
-- obsidian.nvim Mappings
vim.keymap.set("n", "gf", function()
  if require("obsidian").util.cursor_on_markdown_link() then
    return "<cmd>ObsidianFollowLink<CR>"
  else
    return "gf"
  end
end, { noremap = false, expr = true })

vim.keymap.set("n", "nd", "<cmd>NoiceDismiss<CR>", { desc = "Dismiss Noice Message"})

vim.keymap.set('n', 'ns', ':ObsidianSearch<CR>', {silent = true})
vim.keymap.set('n', 'nn', ':ObsidianNew<CR>', {silent = true})
vim.keymap.set('n', 'np', ':ObsidianPasteImage<CR>', {silent = true})
vim.keymap.set('n', 'nr', ':ObsidianRename<CR>', {silent = true})
vim.keymap.set('n', 'nt', ':ObsidianTemplate<CR>', {silent = true})
vim.keymap.set('n', 'nq', ':ObsidianQuickSwitch<CR>', {silent = true})
