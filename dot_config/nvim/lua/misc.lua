local M = {}

-- I dont know what this does or where to catagorize XD
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

function M.insert_template_below()
    local templates_path = vim.fn.expand("~/.nb/theintegrative")
    local line_number = vim.fn.line(".")
    local current_line = vim.fn.getline(line_number)
    local link_start, link_end = string.find(current_line, "%[%[.*%]%]")

    if link_start and link_end then
        local link_text = string.sub(current_line, link_start + 2, link_end - 2)
        local template_path = templates_path .. "/" .. link_text .. ".md"

        if current_line:match('%[%s*%]') then
            if vim.fn.filereadable(template_path) == 1 then
                local template_content = vim.fn.readfile(template_path)
                vim.fn.append(line_number, template_content)
                require("obsidian").util.toggle_checkbox()
            else
                print("Template file does not exist: " .. template_path)
            end
        else
            print("Checkbox already checked: " .. current_line)
        end
    else
        print("No link found on the current line.")
    end
end

return M
