# Dotfiles

Hi! So you landed on my dotfiles. You know what they say (Do they?), judge an engineer by what they do with their dotfiles and beyond. But don't judge me too hard or else I would not be able to push some more updates. 

## Contents
- [chezmoi](https://github.com/twpayne/chezmoi)
- [i3](https://github.com/i3/i3)
- [gh](https://github.com/cli/cli)
- [feh](https://github.com/derf/feh)
- [neovim](https://github.com/neovim/neovim)
- [starship](https://github.com/twpayne/chezmoi)
- [topgrade](https://github.com/topgrade-rs/topgrade)
- [fzf](https://github.com/junegunn/fzf)
- [obs-studio](https://github.com/obsproject/obs-studio)
- [neofetch](https://github.com/dylanaraps/neofetch)
- [git repo manager](https://github.com/dylanaraps/neofetch)
- [basher](https://github.com/basherpm/basher)
- [nb](https://github.com/xwmx/nb)


## Installation 
I manage these configuration using chezmoi!

``` bash
chezmoi init git@gitlab.com:theintegrative/dotfiles.git
```
